package com.artivisi.training.microservice201804.peserta.controller;

import com.artivisi.training.microservice201804.peserta.dao.PesertaDao;
import com.artivisi.training.microservice201804.peserta.entity.Peserta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping("/api/peserta")
public class PesertaApiController {

    @Autowired
    private PesertaDao pesertaDao;

    @GetMapping("/")
    public Iterable<Peserta> semuaPeserta() {
        return pesertaDao.findAll();
    }
}
