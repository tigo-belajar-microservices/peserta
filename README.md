# Aplikasi Peserta Service #

Menyediakan REST API untuk data Peserta

## Deploy Single App ##

1. Buat aplikasi di Heroku, misalnya nama aplikasinya `tms-peserta`

        heroku apps:create tms-peserta

2. Enable Dyno Info melalui CLI

        heroku labs:enable runtime-dyno-metadata

3. Set konfigurasi profile Spring

        heroku config:set SPRING_PROFILES_ACTIVE=heroku

4. Buat database postgresql yang gratisan

        heroku addons:create heroku-postgresql:hobby-dev

5. Tambahkan remote git Heroku kalau belum ada (biasanya otomatis dibuatkan pada waktu menjalankan `heroku apps:create`)

        git remote add heroku https://git.heroku.com/tms-peserta.git

6. Push ke Heroku

        git push heroku master

## Replikasi Aplikasi Peserta ##

1. Buat aplikasi baru di Heroku

        heroku apps:create tms-peserta-1
    
    Outputnya seperti ini
    
        Creating ⬢ tms-peserta-1... done
        https://tms-peserta-1.herokuapp.com/ | https://git.heroku.com/tms-peserta-1.git

2. Aktifkan Dyno Info

        heroku labs:enable runtime-dyno-metadata -a tms-peserta-1

3. Set profile Spring

        heroku config:set SPRING_PROFILES_ACTIVE=heroku -a tms-peserta-1

4. Lihat nama database di aplikasi pertama

        heroku addons -a tms-peserta
    
    Outputnya seperti ini
    
        Add-on                                      Plan       Price  State  
        ──────────────────────────────────────────  ─────────  ─────  ───────
        heroku-postgresql (postgresql-rigid-75959)  hobby-dev  free   created
         └─ as DATABASE
        
        The table above shows add-ons and the attachments to the current app (tms-peserta) or other apps.

5. Pasang database yang sama dengan aplikasi pertama ke aplikasi kedua

        heroku addons:attach postgresql-rigid-75959 -a tms-peserta-1
        
    Outputnya seperti ini
    
        Attaching postgresql-rigid-75959 to ⬢ tms-peserta-1... done
        Setting DATABASE config vars and restarting ⬢ tms-peserta-1... done, v4

6. Tambahkan Git URL heroku aplikasi kedua

        git remote add heroku-1 https://git.heroku.com/tms-peserta-1.git

7. Deploy aplikasi kedua

        git push heroku-1 master